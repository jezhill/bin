Win+E -> View -> Select "Details"
Win+E -> View -> Options
	"General" tab
		Change "Open File Explorer to:" to "This PC"
	"View" tab:
		Press "Apply to folders"
		Uncheck "Hide extensions for known file types"
		Select "Show hidden files, folders and drives"

Microsoft Edge
	Go to google.com
	... -> Settings -> Advanced -> (scroll to bottom) Address bar search -> Change search provider

	Download & install Firefox

Default apps -> Web browser
	Change to Firefox

Night light
	Turn on

Task manager
	More details -> Details tab -> Right-click on "Name" column heading -> Select Columns
		Select: Platform, Elevated, GPU, Image path name, Command line, Description
		Arrange in order:  Name, Platform, Elevated, User name, CPU, GPU, PID, Status, Memory, Image path name, Description, Command line
		Juggle column widths appropriately
	Options -> Select "Hide when minimized"

Right-click on taskbar -> Taskbar settings -> Select which icons appear on the taskbar
	Ensure Task Manager, Volume, Network, Bluetooth are always on
	Turn off most others (maybe turn on cloud-sync Dropbox/OneDrive/Google Drive if used)
	Turn on "Safely remove hardware" once it has become available for the first time
Right-click on taskbar -> Search -> Hidden
Right-click on taskbar -> Uncheck "Show Cortana button"
Right-click and unpin useless taskbar icons (Edge, Shopping, Mail...)
Remove icons from Desktop

About your PC
	Rename this PC (if needed---try to match how the organization names it while being concise & avoiding spaces/specials)
	Reboot if needed

Win+E, Alt+D,  %userprofile%,  drag icon from address bar to top of "Quick access" at side
Win+R -> cmd -> mkdir Documents\%USERNAME%@%COMPUTERNAME%
Right-click on taskbar -> Toolbars -> New toolbar -> Find the empty directory you just made (don't be confused by multiple "Documents" folders if OneDrive is enabled)

Firefox
	Three-bars menu -> Customize -> Bottom bar of window -> Themes -> Dark
	Pin Firefox to taskbar
	Download & install Git
	Download & install 7-zip
	Download & install TortoiseHg
	Download & install PuTTY
	Download & install Anaconda

Git Bash
	ssh-keygen
		(go through it)
	notepad .ssh\id_rsa.pub
		select all, copy, use clipboard contents to create an SSH key on bitbucket.org and/or github.org

	git clone git@bitbucket.org:jezhill/bin
	cd bin/rc
	./link_all.sh
	rm ~/.hgrc
	cp mercurial.ini ~/
	mv ~/.SciTEUser.properties ~/SciTEUser.properties

	git config --global --edit
		uncomment name & email lines
		add [include]\n    path = ~/.gitconfig2 

Puttygen
	Conversions -> Import key -> .ssh -> id_rsa
	(Two-thirds of the way down) -> Save private key -> .ssh -> bitbucket

Anaconda prompt
	mkdir C:\neurotech
	cd C:\neurotech
	hg clone ssh://hg@bitbucket.org/jezhill/FullMonty275
	hg clone ssh://hg@bitbucket.org/jezhill/FullMonty254
	git clone git@bitbucket.org:snapproject/bootstrapping
	git clone git@bitbucket.org:snapproject/audiomath-gitrepo
	git clone git@bitbucket.org:snapproject/shady-gitrepo
	git clone git@bitbucket.org:snapproject/dataflow-gitrepo

	pip install -e bootstrapping
	pip install -e audiomath-gitrepo	
	pip install -e shady-gitrepo
	pip install opencv-python

https://www.bci2000.org/mediawiki/index.php/Programming_Howto:Building_and_Customizing_BCI2000

	TortoiseSVN
	CMake
	Visual Studio
	Qt
	NIDAQ mx
