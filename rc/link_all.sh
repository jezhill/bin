#!/usr/bin/env python


import os
import sys
execName = sys.argv[ 0 ]
execDir, execName = os.path.split( os.path.realpath( os.path.expanduser( execName ) ) )
SLASHDOT = os.path.sep + 'DOT_'
for root, dirs, files in os.walk( execDir ):
	root = os.path.realpath( os.path.expanduser( root ) )
	for file in files:
		target = os.path.join( root, file )
		if SLASHDOT not in target: continue
		link = target[ len( execDir ): ]
		link = link.replace( SLASHDOT, os.path.sep + '.' )
		link = os.path.expanduser( '~' ) + link
		parent = os.path.split( link )[ 0 ]
		if not os.path.exists( parent ): os.makedirs( parent )  # used to call out to mkdir -p but that caused problems on git-bash-for-Windows
		cmd = 'ln -sfn "{target}" "{link}"'.format( target=target, link=link )
		print( cmd )
		os.system( cmd )



