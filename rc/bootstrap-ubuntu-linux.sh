#!/bin/bash
sudo apt-get update
sudo apt-get install \
    vim vim-gtk3 scite                                                                `# editors, for sanity`\
    mercurial git cmake gcc g++                                                       `# essentials for versioning Shady and building ShaDyLib`\
    libglu1-mesa-dev libxrandr-dev libxi-dev libxcursor-dev libxinerama-dev xorg-dev  `# libraries required for building ShaDyLib`\
    curl libudev-dev libtool autotools-dev automake pkg-config                        `# build tools and libraries required for libusb build (part of dpxmode build)`\
    libusb-dev portaudio19-dev libasound-dev                                          `# libraries required for psychtoolbox Python module build`\
    chrome-gnome-shell gnome-shell-extensions gnome-tweaks                            `# facilitate installation and control of gnome shell extensions`\
    libterm-readkey-perl apt-file sakura                                              `# general usability`\
;

# commented out because we should really be using a separate (non-system) distro like anaconda, for development
#sudo apt-get install python python-pip   python-tk                            `# Python 2 basics`
#sudo apt-get install        python3-pip  python3-tk                           `# Python 3 basics`

# get Shady
mkdir -p ~/code
cd ~/code
git clone https://bitbucket.org/snapproject/shady-gitrepo
cd shady-gitrepo
# remember to pip "install" Shady as an editable package

# build the accelerator
#./accel-src/devel/build/go.cmd
# build and incorporate the mode-changer utility for the ViewPixx monitor
#./dpxmode-src/build.cmd
#./dpxmode-src/release.cmd


echo -e '[Desktop Entry]\nType=Application\nIcon=/nonexistent.png\nName=Separator\n' | sudo tee /usr/share/applications/separator.desktop

# In addition, to use Shady on the primary screen, we had to:
# * auto-hide the Ubuntu dock (Applications -> Settings -> Dock -> Auto-hide the Dock)
# 
gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false
gsettings set org.gnome.shell.extensions.dash-to-dock autohide true
# 
# * auto-hide the top bar (search for and install the "Hide Top Bar" extension or
#   go to https://extensions.gnome.org/extension/545/hide-top-bar/  but don't "download":
#   just install the browser extension it suggests, and ensure you have previously done
#   `sudo apt install chrome-gnome-shell` on the command line (don't worry that it says
#   chrome in the name---it will interface with other browsers too). Activate the browser
#   extension and then install the shell extension with the "on" switch on the webpage.
#   Use `gnome-shell --version` to get version information if you need it.
# 
# * If you want to put an empty separator at the top of the dock, run the command above
#   then press the Windows key and search for "separator", right click on the empty icon
#   it finds and say "Add to favorites" (solution adapted from https://askubuntu.com/a/148147 ).


# Get audiomath
cd ~/code
git clone https://bitbucket.org/snapproject/audiomath-gitrepo
cd audiomath-gitrepo
# remember to pip "install" audiomath as an editable package, and also `sudo -H pip install psychtoolbox` if you want

# other generally useful things
pushd ~/bin
platform=`uname|perl -pe tr/A-Z/a-z/`
mkdir -p $platform
cd src
cd crlf
gcc -o ../../$platform/crlf *.c

popd
~/bin/rc/link_all.sh

sudo apt-file update  # just updates the index of apt-file so you can search for dependencies
ssh-keygen -N '' -f ~/.ssh/id_rsa && cat ~/.ssh/id_rsa.pub


# If/when gnome-terminal stops working, try sakura. If it's not already installed, you can press
# ctrl+alt+F3 to get a full-screen terminal, say `sudo apt install sakura`, log out again, and
# ctrl+alt+F2 to get back to the desktop.
# 	To control sakura's default geometry the only option seems to be `sudo vim /usr/share/applications/sakura.desktop` and add -r 40 -c 135   to the Exec line.
#   There are various other (non-geometry) options in ~/.config/sakura/sakura.conf

# Or try roxterm:   https://launchpad.net/~h-realh/+archive/ubuntu/roxterm via https://roxterm.sourceforge.net
sudo add-apt-repository ppa:h-realh/roxterm
sudo apt-get update
sudo apt install roxterm

gsettings set org.gnome.shell.extensions.desktop-icons show-home  false
gsettings set org.gnome.shell.extensions.desktop-icons show-trash false

