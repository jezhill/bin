#!/bin/bash

SUDO=
( which python | grep "^$HOME/" >/dev/null ) || SUDO='sudo -H'

which python

# third-party packages
$SUDO python -m pip install numpy matplotlib ipython pillow opencv-python pyglet pyserial

# shady (editable)
python -m pip install --user -e ~/code/shady-gitrepo

# audiomath (editable) and psychtoolbox (optional, for comparison with/alternative backend for audiomath)
python -m pip install --user -e ~/code/audiomath-gitrepo
$SUDO python -m pip install psychtoolbox

