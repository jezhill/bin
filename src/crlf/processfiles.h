#include <stdarg.h>
#include <stdio.h>

/* public functions provided by processfiles.c */
int isflag(char *arg, char *terse, char *verbose); /* returns 1 if arg matches either of the other two inputs (either may be NULL) */
int warnf(char *fmt, ...); /* prints the executable name followed by a colon, a space, the formatted input, and a newline */
int errorf(char *fmt, ...); /* as per warnf, except that exit(1) is called immediately afterwards */

/* prototypes assumed by processfiles.c ("virtual" functions) */
int islistflag(char *arg); /* should return 1 if arg matches the list flag for this program*/
int matchflags(char *arg); /* should check arg against any other possible flags, store the results as necessary, and return non-zero if match unsuccessful */
int openbinary(void); /* should return 1 if files should be opened in binary mode, 0 if text */
int changefiles(void); /* should return 1 if the program will change the input files, 0 if it only reads them */
int process(FILE *in, FILE *out, int *changed, char *filename); /* Process input stream in into output stream out (out will be NULL
                                                                   if changefiles() returned 0). Set the flag changed if changes were
                                                                   made. Return 1 if error, 0 if none. filename is supplied for purposes
                                                                   of feedback - will be "(stdin)" if stdin */

/*
	Processes files named on command line, overwriting them in place if changes are made.
	If no filenames are supplied, processes stdin to stdout instead. (stdin is ignored if filenames are supplied).
	
	If the "list flag" is used, then a maximum of 1 filename argument may be passed.
	The file (or stdin if no filename supplied) is then treated as a newline-delimited list of files to process.
	Thus, the outputs of ls, find and grep -l can all be piped in, in order to batch-process specified files.	

	Returns number of errors encountered.
*/
