#include "processfiles.h"  /* public header */
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#ifndef _WIN32
#include <unistd.h>
#endif // _WIN32

/* private functions */
int dolist(char *filename); /* NULL for stdin */
int dofile(char *filename);
int vwarnf(char *fmt, va_list ap);

char *gExecName = NULL;

int main(int argc, char *argv[])
{
	int listMode = 0;
	int i, nFiles, returnCode = 0;
	char *arg;
	
	gExecName = argv[0];
	for(nFiles = 0, i = 1; i < argc; i++) {
		arg = argv[i];
		if(arg[0] == '-') {
			if(islistflag(arg)) listMode = 1;
			else if(matchflags(arg) != 0) errorf("unknown option \"%s\"", arg);
		}
		else if(arg[0] != 0) nFiles++;
	}
	if(listMode && nFiles > 1) errorf("list mode cannot be used with more than one input");
	if(!isatty(0) && nFiles > 0) warnf("warning - ignoring stdin");
	
	returnCode = 0; /* number of errors */
	if(nFiles == 0)
		returnCode += (listMode ? dolist(NULL) : dofile(NULL));
	else for(i = 1; i < argc; i++) {
		arg = argv[i];
		if(arg[0] != 0 && arg[0] != '-')
			returnCode += (listMode ? dolist(arg) : dofile(arg));
	}
	return returnCode;
}

int dolist(char *filename)
{
	FILE * fp;
	char c, nameBuf[FILENAME_MAX + 1] = "";
	int np = 0, returnCode = 0;

	if(filename == NULL) fp = stdin;
	else {
		if(filename[0] == 0) return 1;
		if((fp = fopen(filename, "r")) == NULL) {
			warnf("could not open list \"%s\"", filename);
			return 1;
		}
	}
	while(!feof(fp)) {
		c = fgetc(fp);
		if(c == 10 || c == 13 || feof(fp)) {
			nameBuf[np] = 0;
			if(*nameBuf) returnCode += dofile(nameBuf);
			np = 0;
		}
		else {
			nameBuf[np++] = c;
			if(np > FILENAME_MAX) {
				warnf("warning - line too long in file list %s", (filename ? filename : " (stdin)"));
				while(c != 10 && c != 13 && !feof(fp)) c = fgetc(fp);
				nameBuf[np = 0] = 0;
			}
		}
	}
	if(filename) fclose(fp);
	return returnCode;
}
int isflag(char *arg, char *terse, char *verbose)
{
	if(terse && *terse && strcmp(arg, terse) == 0) return 1;
	if(verbose && *verbose && strcmp(arg, verbose) == 0) return 1;
	return 0;
}
int errorf(char *fmt, ...)
{
	int nc;
	va_list ap;
	va_start(ap, fmt);
	nc = vwarnf(fmt, ap);
	va_end(ap);
	exit(1);
	return nc;
}
int warnf(char *fmt, ...) {
	int nc;
	va_list ap;
	va_start(ap, fmt);
	nc = vwarnf(fmt, ap);
	va_end(ap);
	return nc;
}
int vwarnf(char *fmt, va_list ap)
{
	int nc = 0;
	if(gExecName) nc += fprintf(stderr, "%s: ", gExecName);
	nc += vfprintf(stderr, fmt, ap);
	nc += fprintf(stderr, "\n");
	return nc;
}

int dofile(char *filename)
{
	FILE *in = NULL, *out = NULL;
	char *outname = NULL;
	int changed = 0, returnCode;
	int bin, write;
	int removetemp, c;
	
	bin = openbinary();
	write = changefiles();

	if(filename == NULL) {
		in = stdin;
		out = stdout;
	}
	else {
		if((in = fopen(filename, (bin ? "rb" : "r"))) == NULL) {
			warnf("could not open file \"%s\"", filename);
			return 1;
		}
		if(write) {
			if((outname = tmpnam(NULL)) == NULL || *outname == 0) {
				warnf("could not get temporary filename for output");
				fclose(in);
				return 1;
			}
			if((out = fopen(outname, (bin ? "wb" : "w"))) == NULL) {
				warnf("could not open temporary file \"%s\"for output", outname);
				fclose(in);
				return 1;
			}
		}
	}

	returnCode = process(in, out, &changed, (filename ? filename : "(stdin)"));

	removetemp = 0;
	if(outname && *outname) fclose(out);
	if(filename && *filename) fclose(in);
	if(filename && *filename && outname && *outname) {
		if(changed) {
			if(remove(filename) != 0) warnf("could not replace file \"%s\"", filename);
			else if(rename(outname, filename) != 0) {
				switch(errno) {
					case EXDEV: /* system has refused to rename a file across different filesystems: copy the file across byte-by-byte then */
						if((in = fopen(outname, (bin?"rb":"r"))) == NULL) {
							warnf("could not reopen temporary file \"%s\" for input in order to copy across filesystems", outname);
						}
						else if((out = fopen(filename, (bin?"wb":"b"))) == NULL) {
							fclose(in);
							warnf("could not open file \"%s\" for ouput in order to copy across filesystems", filename);
						}
						else {
							while(!feof(in) && (c = fgetc(in))!=EOF) putc(c, out);
							removetemp = 1;
							fclose(out);
							fclose(in);
						}
						break;
					default:
						warnf("could not rename temporary file \"%s\" to \"%s\" - errno %d", outname, filename, errno);	
				}
			}
		}
		else removetemp = 1;
		if(removetemp && remove(outname) != 0) warnf("could not remove temporary file \"%s\"", outname);
	}
	return returnCode;
}

