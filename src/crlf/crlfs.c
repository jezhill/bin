#include "processfiles.h" /* uses the API provided by processfiles.c and processfiles.h */

/*
	0x0D 13 \r CR carriage return
	0x0A 10 \n LF linefeed/newline
	
	Mac uses CR
	Unix uses LF
	DOS uses CR LF
*/

typedef enum {undefined_mode = 0, unix_mode, dos_mode, mac_mode} DestMode;
DestMode gDestMode = undefined_mode;

int islistflag(char *arg) {return isflag(arg, "-l", "--list");}
int matchflags(char *arg)
{
	if(isflag(arg, "-u", "--unix")) {gDestMode = unix_mode; return 0;}
	if(isflag(arg, "-d", "--dos")) {gDestMode = dos_mode; return 0;}
	if(isflag(arg, "-m", "--mac")) {gDestMode = mac_mode; return 0;}
	return 1;
}
int openbinary(void) {return 1;}
int changefiles(void) {return (gDestMode != undefined_mode);}
int process(FILE *in, FILE *out, int *changed, char *filename)
{
	int c, c2, CR = 13, LF = 10;
	int nunix = 0, ndos = 0, nmac = 0;
	DestMode thisLineMode;

	while(!feof(in) && (c = fgetc(in)) != EOF) {
		
		if(c == LF) {
			nunix++;
			thisLineMode = unix_mode;
		}
		else if(c == CR) {
			if(!feof(in)) {
				c2 = fgetc(in);
				if(c2 == LF) {
					ndos++;
					thisLineMode = dos_mode;
				}
				else {
					nmac++;
					thisLineMode = mac_mode;
					ungetc(c2, in);
				}
			}
			else {
				nmac++;
				thisLineMode = mac_mode;
			}
		}
		else {
			if(out && c != EOF) fputc(c, out);
			continue;
		}
		if(out == NULL) continue;
		if(thisLineMode != gDestMode) *changed = 1;
		switch(gDestMode) {
			case unix_mode: fputc(LF, out); break;
			case dos_mode:  fputc(CR, out); fputc(LF, out); break;
			case mac_mode:  fputc(CR, out); break;
			default: break;
		}
	}
	if(gDestMode == undefined_mode) printf("% 5d CRLFs  % 5d CRs  % 5d LFs  in %s\n", ndos, nmac, nunix, filename);
	return 0;
}
