#!/bin/bash

TARGET=$HOME/bin.zip
( cd && rm -f "$TARGET" && find bin -type f -not -path '*/linux/*' -not -path '*/darwin/*' -not -path '*/cygwin/*' | sort | quote | xargs zip "$TARGET" )
ls -AFlh "$TARGET"
